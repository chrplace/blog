(require 'ox-publish)
(setq org-publish-project-alist
  `(("org-files"
     ;; ommited other configurations for brevity
     :base-directory "org/"
     :base-extension "org"
     :recursive t
     :publishing-directory "public_html/"
     :publishing-function org-html-publish-to-html)
    ("static-files"
     :base-directory "org/"
     :base-extension "css\\|js\\|png\\|jpg\\|gif\\|pdf\\|mp3\\|ogg"
     :publishing-directory "public/"
     :recursive t
     :publishing-function org-publish-attachment)
    ("blog" :components ("org-files" "static-files"))))
